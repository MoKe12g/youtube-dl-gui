﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace YouTube_Downloader
{
    public partial class download : Form
    {
        public string doing = "";
        int[] prozent = new int[100];
        public string ordner = "";

        public download()
        {
            InitializeComponent();
        }

        private void download_Load(object sender, EventArgs e)
        {
            int i = 0;
            while (i != 100)
            {
                prozent[i] = i;
                i++;
            }

            using (Process sortProcess = new Process())
            {
                sortProcess.StartInfo.FileName = "YouTube-dl";
                sortProcess.StartInfo.Arguments = doing;
                sortProcess.StartInfo.WorkingDirectory = ordner;
                sortProcess.StartInfo.CreateNoWindow = true;
                sortProcess.StartInfo.UseShellExecute = false;
                sortProcess.StartInfo.RedirectStandardOutput = true;

                // Set event handler
                sortProcess.OutputDataReceived += new DataReceivedEventHandler(SortOutputHandler);

                // Start the process.
                sortProcess.Start();

                // Start the asynchronous read
                sortProcess.BeginOutputReadLine();

                //sortProcess.WaitForExit();
            }
            //timer1.Start();
        }

        void SortOutputHandler(object sender, DataReceivedEventArgs e)
        {
            Trace.WriteLine(e.Data);
            this.BeginInvoke(new MethodInvoker(() =>
            {
                richTextBox1.AppendText("\n");
                richTextBox1.AppendText(e.Data ?? string.Empty);
                if (e.Data == "") this.Text = "Der Thread wurde beendet.";
                if (e.Data != "") this.Text = e.Data;
            }));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string ttext = richTextBox1.Text;
            int i = 0;
            int t = 0;
            int value = 0;
            while (i != 100)
            {
                t = richTextBox1.Text.IndexOf("[download] " + i.ToString());
                if (t > 0) value = i;
                i++;
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Contains("[download] 100% ") == true) this.Text = "Download is ready.";
        }
    }
}
