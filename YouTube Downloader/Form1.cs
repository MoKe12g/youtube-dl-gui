﻿using System;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace YouTube_Downloader
{
    public partial class Form1 : Form
    {

        string UpdateURL = "http://moke12g.de/programme/YouTube_dl_GUI/index.html";  //Update
        string ProgrammVersion = "0.3b";  //Update
        bool notnewversion = true;    //Update
        bool nowupdate = false;   //Update
        bool fensterlaufennoch = false;
        string ffmpegURL = @"https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-3.4.1-win32-static.zip";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            programmstartet t = new programmstartet();
                t.label1.Text = "Downloade youtube-dl";
                t.Update();
                t.Show();
                Directory.CreateDirectory(@"C:\MoKe Programme\youtuble-dl GUI\");
                Environment.CurrentDirectory = @"C:\MoKe Programme\youtuble-dl GUI\";
                WebClient downloader = new WebClient();
                try
                {
                    StreamReader data = new StreamReader(@"C:\MoKe Programme\youtuble-dl GUI\youtube-dl.exe");
                    data.Close();
                }
                catch
                {
                    MessageBox.Show("Please download YouTube dl from the webpage that we open right now and open it with the file selection dialog that appears immediately.", "Important Message");
                    process1.StartInfo.FileName = "https://yt-dl.org/downloads/2018.02.22/youtube-dl.exe";
                    process1.Start();
                    process1.StartInfo.FileName = "YouTube-dl.exe";
                    openFileDialog1.Filter = "Anwendungen | *.exe";
                    DialogResult dr = openFileDialog1.ShowDialog();
                    if (dr == DialogResult.OK) File.Copy(openFileDialog1.FileName, "youtube-dl.exe");
                    openFileDialog1.Filter = null;
                    //downloader.DownloadFile(address: "", fileName: "youtube-dl.exe");
                }
                t.label1.Text = "Check for updates";
                t.Close();
                UpdateCheck(false, false);
                this.Text = "Video Downloader | youtube-dl GUI " + ProgrammVersion;
                textBox2.Text = Environment.CurrentDirectory;
            try
            {
                t.Close();
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string bat = "";
            if (checkBox1.Checked == true) bat = "--write-description --write-info-json --write-annotations ";
            if (checkBox2.Checked == true) bat = bat + "--write-auto-sub ";
            bat = bat + "-i --print-traffic --console-title -k ";
            bat = bat + textBox1.Text;

            download downloade = new download();
            downloade.doing = bat;
            downloade.ordner = textBox2.Text;
            downloade.Show();
            fensterlaufennoch = true;

            //debug MessageBox.Show(bat);
            //process1.StartInfo.WorkingDirectory = textBox2.Text;
            //process1.StartInfo.FileName = "youtube-dl.exe";
            //process1.StartInfo.Arguments = bat;
            //process1.Start();
            textBox1.Text = "YouTube URL";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "YouTube UR") textBox1.Text = "";
        }

        public void UpdateCheck(bool started, bool manuel)
        {
            try
            {
                Systemmessage("Checking for Updates...");
                WebClient UpdateCheckdownloader = new WebClient();
                UpdateCheckdownloader.DownloadFile(UpdateURL, "cache");
                StreamReader CheckUpdateReader = new StreamReader("cache");
                string versioncache = CheckUpdateReader.ReadToEnd();
                CheckUpdateReader.Close();
                notnewversion = versioncache.Contains(ProgrammVersion);
                //MessageBox.Show(notnewversion.ToString());
                if (notnewversion == true)
                {
                    label1.Text = "The program is up to date.";
                    if (manuel == true) MessageBox.Show("The program is up to date.", "Update Manager Version " + ProgrammVersion);
                    Systemmessage("The program is up to date.");
                }
                if (notnewversion == false)
                {
                    Systemmessage("There is a newer version of this program. \nPlease open " + UpdateURL + " go to download the update");
                    DialogResult wupdate = MessageBox.Show("There is a newer version of this program.\nPlease on " + UpdateURL + " go to download the update\nShould this web address be opened?", "Update Manager Version " + ProgrammVersion, MessageBoxButtons.YesNo);
                    process1.StartInfo.FileName = UpdateURL;
                    if (wupdate == DialogResult.Yes) process1.Start();
                    if (wupdate == DialogResult.No) Systemmessage("Update was rejected by the user");
                    if (wupdate == DialogResult.No) label1.Text = "Please update via the Update Checker!";
                    if (wupdate == DialogResult.Yes && started == false) nowupdate = true;
                    if (wupdate == DialogResult.Yes) Close();
                    //toolStripStatusLabel4.Text = "Du bist online.";
                    //toolStripStatusLabel4.ForeColor = Color.Green;
                }
            }
            catch
            {
                Systemmessage("It could not connect to the web server " + UpdateURL + " being constructed. Current program version: " + ProgrammVersion);
                if (manuel == true) MessageBox.Show("It was not possible to search for updates, please check your internet connection.", "Update Manager Version " + ProgrammVersion);
                label1.Text = "It could not connect to the web server " + UpdateURL + " be set up, please check your internet connection. Program Version: " + ProgrammVersion;
                //toolStripStatusLabel4.Text = "Du bist OFFLINE";
                //toolStripStatusLabel4.ForeColor = Color.Orange;
                notnewversion = true;
            }
            File.Delete("cache");
        }

        public void Systemmessage(string messageText)
        {
            Console.WriteLine(DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + ":" + DateTime.Now.Millisecond + " FunWriter " + ProgrammVersion + ":\t" + messageText);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = textBox2.Text;
            DialogResult zt = folderBrowserDialog1.ShowDialog();
            if (zt == DialogResult.OK) textBox2.Text = folderBrowserDialog1.SelectedPath;
        }

        private void process1_Exited(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string t = openFileDialog1.FileName;
            saveFileDialog1.ShowDialog();
            string t2 = saveFileDialog1.FileName;
            string bat = "-i \"" + t + "\" -codec:a libmp3lame -qscale:a 2 \"" + t2 + "\"";
            //MessageBox.Show(bat);
            //textBox1.Text = bat;
            process1.StartInfo.FileName = "ffmpeg.exe";
            process1.StartInfo.Arguments = bat;
            process1.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Webseite URL") textBox1.Text = "";
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "") textBox1.Text = "Webseite URL";
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            YouTube_dl_GUI.Choose form = new YouTube_dl_GUI.Choose();
            form.ShowDialog();
            if (form.Text != "Choose a Video") textBox1.Text = form.Text;
        }
    }
}